using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Interfaces;
using Microsoft.Extensions.Logging;

namespace Application.Services
{
    public class BaseService<T> : IService<T> where T : class
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<BaseService<T>> _logger;

        public BaseService(IUnitOfWork unitOfWork, IMapper mapper, ILogger<BaseService<T>> logger)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            try
            {
                var entities = await _unitOfWork.GetRepository<T>().GetAllAsync();
                return _mapper.Map<IEnumerable<T>>(entities);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while getting all entities.");
                throw;
            }
        }

        public async Task<T> GetByIdAsync(int id)
        {
            try
            {
                var entity = await _unitOfWork.GetRepository<T>().GetByIdAsync(id);
                return _mapper.Map<T>(entity);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occurred while getting entity by id {id}.");
                throw;
            }
        }

        public async Task AddAsync(T entity)
        {
            try
            {
                var entityToAdd = _mapper.Map<T>(entity);
                await _unitOfWork.GetRepository<T>().AddAsync(entityToAdd);
                await _unitOfWork.CommitAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while adding an entity.");
                throw;
            }
        }

        public async Task UpdateAsync(T entity)
        {
            try
            {
                var entityToUpdate = _mapper.Map<T>(entity);
                await _unitOfWork.GetRepository<T>().UpdateAsync(entityToUpdate);
                await _unitOfWork.CommitAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while updating an entity.");
                throw;
            }
        }

        public async Task DeleteAsync(int id)
        {
            try
            {
                await _unitOfWork.GetRepository<T>().DeleteAsync(id);
                await _unitOfWork.CommitAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error occurred while deleting entity with id {id}.");
                throw;
            }
        }

        public async Task<IEnumerable<T>> FindAsync(Expression<Func<T, bool>> predicate)
        {
            try
            {
                var entities = await _unitOfWork.GetRepository<T>().FindAsync(predicate);
                return _mapper.Map<IEnumerable<T>>(entities);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while finding entities.");
                throw;
            }
        }

        public async Task<int> CountAsync()
        {
            try
            {
                return await _unitOfWork.GetRepository<T>().CountAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while counting entities.");
                throw;
            }
        }

        public async Task<bool> ExistsAsync(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return await _unitOfWork.GetRepository<T>().ExistsAsync(predicate);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occurred while checking existence of entities.");
                throw;
            }
        }
    }
}
