using Application.Dtos;
using AutoMapper;
using Domain.Entities;

namespace Application.Mappings
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Adicione seus mapeamentos aqui
            // CreateMap<Source, Destination>();
            CreateMap<Product, ProductDto>().ReverseMap();

        }
    }
}
