# MCF API

Este projeto é uma API construída em ASP.NET Core 8, aplicando os princípios S.O.L.I.D. e utilizando AutoMapper, Entity Framework Core e Swagger para documentação.

## Estrutura do Projeto

O projeto está organizado em quatro camadas principais:

- **Api**: Contém os controladores e configurações específicas da API.
- **Domain**: Contém as entidades, interfaces e regras de negócio.
- **Infrastructure**: Contém a implementação dos repositórios, contextos de banco de dados e mapeamentos.
- **Application**: Contém os serviços de aplicação e mapeamentos do AutoMapper.

## Requisitos

- Docker Desktop (baixar e instalar para execução em contêiner)
- Executar o Docker Desktop

## Configuração

### Clonar o Repositório

```sh
git clone https://gitlab.com/marcelocosta/mydotnet
cd mydotnet

### Executar aplicação
docker-compose up
ou
docker-compose up --build




- **Comandos para executar o projeto fora dos containers docker** -

dotnet restore
dotnet build (opcional)
dotnet run -p api


















Mudanças e Melhorias
Centralização da Lógica de Acesso a Dados:

BaseRepository: A implementação do BaseRepository centraliza as operações básicas de CRUD (Create, Read, Update, Delete) e consultas, evitando duplicação de código e promovendo reutilização.

UnitOfWork: A implementação do UnitOfWork fornece um ponto único para gerenciar transações e repositórios, garantindo que todas as operações de banco de dados dentro de uma mesma unidade de trabalho sejam tratadas de forma consistente.




Organização e Separação de Responsabilidades:

As interfaces e classes estão organizadas de forma que cada camada (Domain, Application, Infrastructure) tenha responsabilidades claras e bem definidas.

Domain: Contém as interfaces que definem contratos, como IBaseRepository, IUnitOfWork, e IService.

Infrastructure: Contém as implementações concretas dessas interfaces, como BaseRepository e UnitOfWork.

Application: Contém os serviços de aplicação que utilizam essas implementações para executar lógica de negócios.




Facilidade de Testes e Manutenção:

A separação clara entre interfaces e implementações facilita a substituição de implementações e o uso de mocks/stubs para testes.
O código é mais modular e fácil de manter, com responsabilidades bem definidas e centralização da lógica de acesso a dados.










Resumo

BaseRepository: Prove operações CRUD genéricas e consultas para qualquer entidade. Utilizado para centralizar e reutilizar a lógica de acesso a dados.

UnitOfWork: Gerencia repositórios e garante a consistência das transações. Utilizado para garantir que todas as operações de banco de dados em uma unidade de trabalho sejam tratadas de forma consistente.

Organização: A separação de responsabilidades e a modularidade do código facilitam a manutenção, testes e extensão da aplicação.
Essas práticas e implementações garantem que sua aplicação seja mais fácil de manter, testar e escalar.